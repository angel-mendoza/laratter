<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);


        DB::table('users')->insert([
          'name' =>'Angel Mendoza',
          'username' =>'angel_mendoza',
          'email' =>'test@gmail.com',
          'password' => bcrypt('123'),
          'avatar' => 'http://api.adorable.io/avatars/285/'.random_int(1, 1000).'.png',
          'created_at' => now(),
          'updated_at' => now()
    	]);

    }
}

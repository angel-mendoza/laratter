<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function all()
    {

        // $messages = DB::table('messages')
        // ->orderBy('created_at', 'desc')
        // ->get();       

        $messages = DB::table('messages')
        ->join('users', 'users.id', '=', 'messages.user_id')
        ->select('users.name', 'users.username', 'users.email', 'users.avatar', 'messages.*' )
        ->orderBy('messages.created_at', 'desc')
        ->get();

        
        return response()->json([
            "messages" => $messages
        ], 200);
    }

    public function new()
    {
        //$credentials = $request;
        $content = request(['content']);
        $user_id = request(['user_id']);

        //dd($credentials['content']);
        
        $message = new Message();
        $message->content = $content['content'];
        $message->user_id = $user_id['user_id'];
        $message->image = "";
        
        $message->save();

        return response()->json([
            "message" => $message
        ], 200);      
    }
}

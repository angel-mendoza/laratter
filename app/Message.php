<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'content', 'image' , 'user_id'
    ];

    public function user()
    {
    	//return $this->belongsTo(User::class);
    	return $this->belongsTo('App\User');
    }
}

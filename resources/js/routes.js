import Home from './components/sections-app/home.vue'
import Dashboard from './components/sections-app/dashboard.vue'


export const routes = [
	{
		path: '/',
		component: Home
	},
	{
		path: '/home',
		component: Dashboard,
		meta: {
			requiresAuth: true
		}			
	}	
]
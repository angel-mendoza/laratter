import { getLocalUser } from "./helpers/auth";
import { setAuthorization } from "./general";


const user = getLocalUser();

export default {
	state: {
        nameApp: "Laratter",
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        messages: []
	},
	getters: {
        getNameApp(state) {
            return state.nameApp
        },
        getCurrentUser(state) {
            return state.currentUser
        },
        isLoading(state) {
            return state.loading
        },
        isLoggedIn(state) {
            return state.isLoggedIn
        },
        getMessages(state){
            return state.messages
        }           		
	},
	mutations: {
        login(state) {
            state.loading = true
        },
        register(state) {
            state.loading = true
        },        
        loginSuccess(state, payload) {
            state.isLoggedIn = true
            state.loading = false
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token})
            localStorage.setItem("user", JSON.stringify(state.currentUser))
        },
        loginFailed(state) {
            state.loading = false;
            state.isLoggedIn = false
        },
        logout(state) {
            localStorage.removeItem("user")
            state.isLoggedIn = false
            state.currentUser = null
            state.messages = null
        },
        registerSuccess(state, payload) {
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});
            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        registerFailed(state){
            state.loading = false;
            state.isLoggedIn = false
        },
        getMessages(state , payload){
            state.messages = payload
        }          		
	},
	actions: {
        login(context , payload){
            return new Promise((res,rej)=>{
                axios.post('/api/auth/login', payload)
                    .then((response) =>{
                        setAuthorization(response.data.access_token)
                        res(response.data)
                    })
                    .catch((err) =>{
                        rej("something went wrong in the Email or Password");
                    })                    
            })
        },
        register(context , payload) {
            return new Promise((res, rej) => {
                axios.post('/api/auth/register', payload)
                    .then((response) => {
                        setAuthorization(response.data.access_token)
                        res(response.data)

                    })
                    .catch((err) =>{
                        rej("Something went wrong, try it later!");
                    })
            })
        },	
        getMessages(context){
            axios.get('/api/message/all', {
                headers:{
                    "Authorization": `Bearer ${context.state.currentUser.token}`
                }
            })
            .then( (response) => {
                context.commit('getMessages', response.data.messages)
            })
        },    
        createMessage(context , payload) {
            return new Promise((res, rej) => {
                axios.post('/api/message/new', payload , {
                    headers:{
                        "Authorization": `Bearer ${context.state.currentUser.token}`
                    } 
                })
                    .then((response) => {
                        setAuthorization(context.state.currentUser.token)
                        res("successfully saved message")

                    })
                    .catch((err) =>{
                        rej("Something went wrong, try it later!");
                    })
            })
        },              	
	}
}
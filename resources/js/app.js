require('./bootstrap');
import	Vue from 'vue'
import	Vuex from 'vuex'
import	VueRouter from 'vue-router'
import {routes} from './routes'
import StoreData from './store'
import MainApp from './components/sections-app/mainApp.vue'
import {initialize} from './general'
import VueSweetalert2 from 'vue-sweetalert2'

const options = {
	confirmButtonColor: '#41b882',
	cancelButtonColor: '#ff7674'
}

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueSweetalert2, options)

const store = new Vuex.Store(StoreData)

const router = new VueRouter({
	routes,
	mode: 'history'
})

initialize(store, router)

const app = new Vue({
    el: '#app',
    router,
    store,
    components:{
    	MainApp
    }
});
